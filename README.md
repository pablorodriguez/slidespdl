## Introducción a PDL

Presentación creada para la [reunión de octubre de 2015](http://www.meetup.com/Madrid-Perl-Mongers/events/225918843/) de [Madrid.pm](http://madrid.pm.org/)

## Utilización

```
#!bash
cpanm Carton
carton
carton exec revealup serve slides.md --theme sky.css

```