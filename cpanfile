requires 'PDL', '<=2.013'; # A partir de la versión 2.014 se pueden usar piddles de más de 2^32 y algunos módulos no estarán todavía adaptados
requires 'PDL::AutoLoader';
requires 'PDL::Stats';
requires 'PDL::IO::CSV';
requires 'PDL::Graphics::Simple';
requires 'PDL::Graphics::Gnuplot';
requires 'Data::TestImage';


# Para la presentación:
requires 'App::revealup', '0.21';

