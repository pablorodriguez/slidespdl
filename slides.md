## Introducción a PDL
## Madrid.pm
## 2015-10-20

---

## Qué

- Herramienta para manipulación eficiente de datos (especialmente números) <!-- .element: class="fragment" data-fragment-index="1" -->
- Compuesto por un conjunto de módulos Perl <!-- .element: class="fragment" data-fragment-index="2" -->
- Lenguaje de programación basado en arrays <!-- .element: class="fragment" data-fragment-index="3" -->

---

## Sirve para...

* Cálculo científico
    - Algebra lineal
    - Cálculo numérico
    - Estadística
* Visualización de datos
* Procesado de imágenes

---

## Perl también tiene arrays...

* Pero solo de una dimensión
* Los arrays en Perl son estructuras no triviales (2D: listas de listas)
* La auto-vivificación es costosa
* Es de propósito general

---

## Expresivo, rápido y eficiente

* [TIMTOWTDI](https://en.wikipedia.org/wiki/There%27s_more_than_one_way_to_do_it): imperativo, funcional, pipeline
* Implementado en C
* Se trabaja con muchos datos a la vez

---
## Avisos

* PDL lo empezó [Karl Glazebrook](https://en.wikipedia.org/wiki/Karl_Glazebrook) para hacer astronomía
* Manejo de grandes cantidades de datos (manipulación de imágenes, algebra, cálculo numérico, ...)
* [Guía para usuarios de Matlab](https://metacpan.org/pod/PDL::MATLAB)
* [Guía para usuarios de Scilab](https://metacpan.org/pod/PDL::Scilab)

---

## Utilización
___

## Entidad básica: "Piddle"

* Es un buffer de C, un puntero
* Región de memoria contigua
* Se utiliza para almacenar colecciones de datos N-dimensionales
* Puede contener los tipos fundamentales pero también definidos por el usuario
* En realidad es un objeto con el buffer y la información necesaria para trabajar cómodamente
___

## Uso básico: construcción

Multitud de constructores

```perl
my $zeros = zeros(5, 5);
my $sequence_1d = sequence(101);

my $piddle_vector_from_string = pdl('[1 2 3 4]');
my $piddle_matrix_from_string = pdl('[1 2 3; 4 5 6]');

my $scalar = pdl atan(1)*4;

```
___

## Uso básico: operaciones aritméticas

```perl
my $ones = ones(3, 4);
my $twos = $ones + ones($ones->dims);
my $fours = $twos * $twos;
```
___

## Tipo de dato

Los piddle por defecto son de doble precisión

```perl
my $byte_piddle = bytes(ones(10, 10));
my $integer_piddle = long([4..14]);
my $float_piddle = float(zeros(4, 3));
```
___
## Mezclando arrays y piddles

```perl
my $piddle_from_arrayref = pdl([1..5]);
my $piddle_from_array = pdl(1..5);
my $piddle_from_2d_array = pdl([[1, 2, 3], [4, 5, 6]]);
```
___

## Utilización en scripts

```perl
use PDL;
```
Cuando el tiempo de carga es un problema:

Orientado a objetos

```perl
use PDL::Lite; 
```

Funciones

```perl
use PDL::LiteF;  
```

___

## Depuración

[PDLdb.pl](https://metacpan.org/pod/distribution/PDL/PDLdb.pl)<!-- .element: class="fragment" data-fragment-index="1" -->

No lo recomiendo directamente<!-- .element: class="fragment" data-fragment-index="2" -->

Alternativas: <!-- .element: class="fragment" data-fragment-index="3" --> 
* Utilizando la vista: Perl Expression View de Eclipse EPIC<!-- .element: class="fragment" data-fragment-index="4" -->
* Entorno interactivo y visualización gráfica <!-- .element: class="fragment" data-fragment-index="5" -->

___

## Utilización interactiva

Entornos [REPL](https://en.wikipedia.org/wiki/Read%E2%80%93eval%E2%80%93print_loop):

* [perldl](https://metacpan.org/pod/distribution/PDL/perldl)
* [pdl2](https://metacpan.org/pod/distribution/PDL/Perldl2/pdl2)
* [Devel::REPL](https://metacpan.org/pod/Devel::REPL)
* [Reply](https://metacpan.org/pod/Reply)
* [Devel::IPerl](https://metacpan.org/pod/IPerl)

---

## Entrada de datos

PDL tiene soporte para formatos científicos: [PDL::IO](https://metacpan.org/pod/PDL::IO)

* NDF, HDF, HDF5, IDL
* NetCDF
* RAW, FastRaw
* Imágenes
* ASCII

___

### ASCII

Un formáto tipico: CSV

Forma recomendada: [PDL::IO::CSV](https://metacpan.org/pod/PDL::IO::CSV)
Forma clásica: [rcols](https://metacpan.org/pod/PDL::IO::Misc#rcols)

```perl
use PDL;
use PDL::IO::CSV qw(:all);
use Data::Dumper;

my ($column1, $column2, $column3) = rcsv1D('morley.csv', {header => 1});
print Dumper($column3->hdr);

my $data = rcsv2D('file.csv', {type => ['datetime', long]});

```

___

## Imágenes

[PDL::IO::Pic](https://metacpan.org/pod/PDL::IO::Pic)

```perl
use PDL;
use PDL::IO::PIC;
use Data::TestImage;

my $image = 
    rpic(
        Data::TestImage->get_image('mandrill')->absolute->stringify);

print join ' ', $image->dims; # 3 512 512

```

---

## A Calcular!

___

## Creación de funciones

Los piddles no son más que objetos perl, asique una función que trabaje con piddles es igual que una que use objetos (referencias). Para evitar sustos podemos usar: topdl

```perl
sub ErrorMedioAbsoluto {
    my $valor_real = topdl(shift);
    my $valor_estimado = topdl(shift);
    return avg(abs($valor_real - $valor_estimado);
}

```
___

## Bad Values

En la vida real siempre falta algún dato. <!-- .element: class="fragment" data-fragment-index="1" -->

PDL lo resuelve por tí... <!-- .element: class="fragment" data-fragment-index="2" -->

pero tiene un impacto en el rendimiento. <!-- .element: class="fragment" data-fragment-index="3" -->

```perl
pdl> p $PDL::Bad::Status
1
pdl> $a = sequence(4,3);
pdl> p $a
[
 [ 0  1  2  3]
 [ 4  5  6  7]
 [ 8  9 10 11]
]
pdl> $a = $a->setbadif( $a % 3 == 2 )
pdl> p $a
[
 [  0   1 BAD   3]
 [  4 BAD   6   7]
 [BAD   9  10 BAD]
]
``` 
<!-- .element: class="fragment" data-fragment-index="4" -->
___

## Matrices

Cualquier piddle bidimensional puede ser una matriz, pero es más fácil con: [PDL::Matrix](https://metacpan.org/pod/PDL::Matrix)

```perl
use PDL::Matrix;
 
$m = mpdl [[1,2,3],[4,5,6]];
$m = PDL::Matrix->pdl([[1,2,3],[4,5,6]]);
$m = msequence(4,3);
```

___

## Slicing

Los piddles se pueden recorrer y acceder de muchas formas. En realidad son una región de memoria contigua.

Podemos decidir libremente como los recorremos y como accedemos a ellos.

Podemos acceder solo a una ventana o recorrerlos al revés, lo que estaremos creando son "vistas". A efectos prácticos piddles enlazados al piddle que contiene los datos de verdad.

___

## Threading

Un buen lenguaje orientado a arrays (como PDL) nos permite no tener que escribir bucles. 

Básicamente se hace un bucle automáticamente sobre las dimensiones que no queremos operar. Por ejemplo al sumar dos piddles.

Obligatorio en aplicaciones de alto rendimiento: [PDL::Threading](https://metacpan.org/pod/distribution/PDL/Basic/Pod/Threading.pod)

___

## Fechas y horas

[PDL::DateTime](https://metacpan.org/pod/PDL::DateTime)

___

## Estadística

[PDL::Stats](https://metacpan.org/pod/PDL::Stats)
[Statistics::NiceR](https://metacpan.org/pod/Statistics::NiceR)

___

## DataFrame

[Data::Frame](https://metacpan.org/pod/Data::Frame)

---

## Visualización

Varias posibilidades

* [PDL::Graphics::Simple](https://metacpan.org/pod/PDL::Graphics::Simple)
* [PDL::Graphics::Gnuplot](https://metacpan.org/pod/PDL::Graphics::Gnuplot)
* [PDL::Graphics::PGPLOT](https://metacpan.org/pod/PDL::Graphics::PGPLOT)
* [PDL::Graphics::TriD](https://metacpan.org/pod/PDL::Graphics::TriD)

Recomendable: demo en pdl2
___

## GNUPlot

```perl
use PDL;
use PDL::Graphics::Gnuplot;
use DateTime;

my $array = pdl [ map { rand 1000 } 0 .. 1000 ];
my $now = DateTime->now()->truncate(to => 'hour')->epoch();
my $datetimes = pdl [ map { $now += 3600 } 0 .. 1000 ];

gplot(terminal => 'qt', 
      xdata => 'time',
      xtics => { format => '%Y-%m-%d %H:%M:%S' }, 
      with => 'lines', 
      $datetimes, 
      $array );

gplot(terminal => 'qt', 
      xdata => 'time', 
      xtics => { format => '%Y-%m-%d %H:%M:%S' }, 
      with => 'lines', 
      $datetimes,
      random(1000)*1000 );

```
___

## Bokeh con IPerl

http://nbviewer.ipython.org/github/zmughal/zmughal-iperl-notebooks/blob/master/IPerl-demos/20150322_BokehJS_plotting_image.ipynb

---

## Salida de datos

___

## CSV

[PDL::IO::CSV](https://metacpan.org/pod/PDL::IO::CSV)

```perl
use PDL;
use PDL::IO::CSV qw(:all);

my $column = sequence(140);
wcsv1D($column, 'sequence.csv');

my $normal = grandom(10, 10);
$normal->wcsv2D('normal.csv');

```
___

## FastRAW

Es el más rápido, pero solo para nuestra máquina

```perl
use PDL;
use PDL::IO::FastRaw;
 
writefraw($pdl,"fname");         # write a raw file
 
$pdl2 = readfraw("fname");       # read a raw file
$pdl2 = PDL->readfraw("fname");
 
$pdl3 = mapfraw("fname2",{ReadOnly => 1}); # mmap a file, don't read yet
 
$pdl4 = maptextfraw("fname3",{...}); # map a text file into a 1-D pdl.
```

---

## Llamada a otras librerías

___

## Interfaz con C

* PDL::PP
* Inline qw(Pdlpp)

---
## Otros módulos/sitios para cálculo científico con Perl

* [Perl 4 Science](http://perl4science.github.io)
* [BioPerl](https://metacpan.org/pod/distribution/BioPerl/BioPerl.pm)
* [SOOT](https://metacpan.org/pod/SOOT)
* [The Quatified Onion](https://groups.google.com/forum/#!forum/the-quantified-onion)

---

## Conclusión

* PDL es muy versátil y potente
* Sigue en desarrollo [Página de Github](https://github.com/PDLPorters/pdl/graphs/contributors)
* Usadlo!

Material: https://bitbucket.org/pablorodriguez/slidespdl
